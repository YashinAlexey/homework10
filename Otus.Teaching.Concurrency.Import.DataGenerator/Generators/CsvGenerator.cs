﻿using CsvHelper;
using CsvHelper.Configuration;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using System.Globalization;
using System.IO;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    class CsvGenerator : IDataGenerator
    {
        private readonly string _fileName;
        private readonly int _dataCount;

        public CsvGenerator(string fileName, int dataCount)
        {
            _fileName = fileName;
            _dataCount = dataCount;
        }

        public void Generate()
        {
            CsvConfiguration configuration = new CsvConfiguration(CultureInfo.CurrentCulture);
            configuration.HasHeaderRecord = false;
            configuration.Delimiter = ";";

            using (StreamWriter streamWriter = new StreamWriter(_fileName))
            {
                using (CsvWriter csvWriter = new CsvWriter(streamWriter, configuration))
                {
                    csvWriter.WriteRecords(RandomCustomerGenerator.Generate(_dataCount));
                }
            }
        }
    }
}
