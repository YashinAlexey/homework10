using System;
using System.Threading;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;
using System.Diagnostics;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class XmlDataLoader : IDataLoader 
    {
        private readonly CustomersList _customers;
        private readonly int _threadsCount;
        private readonly int _attemptNumber;
        private readonly bool _usingThreadPool;

        public XmlDataLoader(CustomersList customers, int threadsCount, int attemptNumber, bool usingThreadPool = true)
        {
            _customers = customers;
            _threadsCount = threadsCount;
            _attemptNumber = attemptNumber;
            _usingThreadPool = usingThreadPool;
        }

        public void LoadData()
        {
            if (_customers.Customers.Count == 0 || _threadsCount == 0)
                return;
            
            Console.WriteLine("Loading data...");
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            ParameterizedThread[] parameterizedThreads = new ParameterizedThread[_threadsCount];
            int pageSize = (int)Math.Ceiling((double)_customers.Customers.Count / (double)_threadsCount);
            int page = 1;
            int skipEntries = 0;

            if (_usingThreadPool)
                ThreadPool.SetMinThreads(_threadsCount, _threadsCount);

            while (skipEntries < _customers.Customers.Count)
            {
                ThreadParameters threadParameters = new ThreadParameters(_customers.Customers.Select(c => c)
                                                                        .Skip(skipEntries)
                                                                        .Take(pageSize)
                                                                        .ToList(), _attemptNumber);
                parameterizedThreads[page - 1] = GetNewThread(threadParameters);
                page++;
                skipEntries = pageSize * (page - 1);
            }

            CheckExecutionResults(parameterizedThreads);

            stopWatch.Stop();
            Console.WriteLine("Loaded data...");
            Console.WriteLine($"Elapsed time: {stopWatch.ElapsedMilliseconds} ms");
        }

        private ParameterizedThread GetNewThread(ThreadParameters threadParameters)
        {
            ParameterizedThread parameterizedThread = new ParameterizedThread(threadParameters);

            if (_usingThreadPool)
            {                
                ThreadPool.QueueUserWorkItem(LoadDataset, threadParameters);
            }
            else
            {            
                Thread thread = new Thread(new ParameterizedThreadStart(LoadDataset));
                thread.Start(threadParameters);
            }

            return parameterizedThread;
        }

        private void LoadDataset(object loadParameters)
        {
            ThreadParameters parameters = (ThreadParameters)loadParameters;
            parameters.WaitHandler.WaitOne();
            WaitRerun:
            parameters.RerunHandler.WaitOne();
            try
            {
                CustomerRepository customerRepository = new CustomerRepository();
                customerRepository.AddCustomers((List<Customer>) parameters.Customers);
            }
            catch (Exception exception)
            {
                parameters.Successfully = false;                 
            }
            finally
            {
                --parameters.AttemptNumber;
                parameters.WaitHandler.Set();
            }

            if (parameters.Successfully || parameters.AttemptNumber == 0)
                parameters.RerunHandler.Set();
            else
                goto WaitRerun;
        }

        private void CheckExecutionResults(ParameterizedThread[] parameterizedThreads)
        {
            AutoResetEvent.WaitAny(parameterizedThreads.Select(pt => pt.ThreadParameters.WaitHandler).ToArray());

            IEnumerable<AutoResetEvent> rerunHandlers =
                from pt in parameterizedThreads
                where !pt.ThreadParameters.Successfully & pt.ThreadParameters.AttemptNumber > 0
                select pt.ThreadParameters.RerunHandler;

            if (rerunHandlers.Count() == 0)
                return;

            foreach (AutoResetEvent rerunHandler in rerunHandlers)
                rerunHandler.Set();

            CheckExecutionResults(parameterizedThreads);
        }                
    }
}