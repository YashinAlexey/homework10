﻿using System;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.ConsoleClient
{
    class Program
    {
        static void Main(string[] args)
        {
            CustomerClient customerClient = new CustomerClient();

            var customers = RandomCustomerGenerator.Generate(2);
            foreach (var customer in customers)
            {
                customer.Id = 0;
                Task<Uri> result = customerClient.CreateCustomerAsync(customer);                
            }                
        }
    }
}
