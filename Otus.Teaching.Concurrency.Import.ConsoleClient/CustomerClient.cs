﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.ConsoleClient
{
    class CustomerClient
    {
        private HttpClient _httpClient;

        public CustomerClient()
        {
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = new Uri("https://localhost:5001");
            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<Uri> CreateCustomerAsync(Customer customer)
        {
            HttpResponseMessage response = await _httpClient.PostAsJsonAsync(
                "api/Customers", customer);
            //response.EnsureSuccessStatusCode();            

            return response.Headers.Location;
        }

        public async Task<Uri> PutCustomerAsync(Customer customer)
        {
            HttpResponseMessage response = await _httpClient.PutAsJsonAsync(
                "api/Customers", customer);
            //response.EnsureSuccessStatusCode();            

            return response.Headers.Location;
        }

        public async Task<Customer> GetCustomerAsync(string path)
        {
            Customer customer = null;
            HttpResponseMessage response = await _httpClient.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                customer = await response.Content.ReadAsAsync<Customer>();
            }
            return customer;
        }
    }
}
